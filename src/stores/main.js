import { defineStore } from "pinia";
import { useStorage } from "@vueuse/core";
import { numbers } from "../lib/numbers";

export const useStore = defineStore("main", {
  state: () => ({
    unpickedNumbers: useStorage("unpickedNumbers/v1", [...numbers]),
    pickedNumbers: useStorage("pickedNumbers/v1", []),
    lastPickedNumber: useStorage("lastPickedNumber/v1", null),
    continueGame: true,
  }),
  getters: {
    isPaused() {
      return !this.continueGame;
    },
  },
  actions: {
    roll() {
      if (this.unpickedNumbers.length > 0) {
        let unpickedIndex = Math.floor(
          Math.random() * this.unpickedNumbers.length
        );

        if (unpickedIndex !== -1) {
          let pickedNumber = this.unpickedNumbers[unpickedIndex];

          this.pickedNumbers.push(pickedNumber);
          this.lastPickedNumber = pickedNumber;
          this.unpickedNumbers.splice(unpickedIndex, 1);
        }
      }
    },
    reset() {
      this.unpickedNumbers = [...numbers];
      this.pickedNumbers = [];
      this.lastPickedNumber = null;
    },
    pause() {
      this.continueGame = false;
    },
    continue() {
      this.continueGame = true;
    },
  },
});
